{% from "pindy_dotfiles/map.jinja" import pindy_dotfiles with context %}

{% set username = salt['pillar.get']('pindy_dotfiles:username', 'pindy') %}
{% set home = "/home/%s" % username %}

pindy_dotfiles_plasma_face_managed:
  file.managed:
    - name: {{ home }}/.face
    - source: https://files.inventtheworld.com.au/index.php/s/QsXjLr9AmQAp2eb/download
    - source_hash: b3c9181800ea74796363e46c0a19c36c0d512d864700b5a010f297cfc868841f19ede1a789eb57a7199fa6af868aa053c37c2e9251860f19a8c21dadd083228b
    - show_changes: False
    - user: {{ username }}
    - group: {{ username }}
    - mode: {{ 644 }}
    - require:
      - user: {{ username }}

pindy_dotfiles_plasma_face_icon_symlink_managed:
  file.symlink:
    - name: {{ home }}/.face.icon
    - target: {{ home }}/.face
    - user: {{ username }}
    - group: {{ username }}
    - require:
      - user: {{ username }}
      - pindy_dotfiles_plasma_face_managed

pindy_dotfiles_plasma_kwinrc_managed:
  file.managed:
    - name: {{ home }}/.config/kwinrc
    - user: {{ username }}
    - group: {{ username }}
    - makedirs: True
    - replace: False
    - require:
      - user: {{ username }}

{{ username }}_dotfiles_plasma_kwinrc_config_settings_managed:
  ini.options_present:
    - name: {{ home }}/.config/kwinrc
    - sections:
        Compositing:
          OpenGLIsUnsafe: false
        Desktops:
          Number: 1
          Rows: 1
        Windows:
          AutoRaise: false
          FocusPolicy: FocusFollowsMouse
          FocusStealingPreventionLevel: 1
    - require:
      - {{ username }}_dotfiles_plasma_kwinrc_managed
