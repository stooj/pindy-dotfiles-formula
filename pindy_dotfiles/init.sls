include:
  - .essentials
  - .bash
  - .bin-files
  - .git
  {% if salt['grains.get']('role', '') == 'personal-computer' %}
  - .gpg
  - .invent
  - .nextcloud
  - .pass
  - .plasma
  - .printer
  {% endif %}
