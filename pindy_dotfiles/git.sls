{% from "pindy_dotfiles/map.jinja" import pindy_dotfiles with context %}

pindy_dotfiles_git_installed:
  pkg.installed:
    - pkgs: {{ pindy_dotfiles.pkgs.git|yaml }}

{% for key, value in pindy_dotfiles.gitconfig.items() %}
pindy_dotfiles_gitconfig_{{ key }}:
  git.config_set:
    - name: {{ key }}
    - value: {{ value }}
    - user: {{ pindy_dotfiles.username }}
    - global: True
    - require:
      - user: {{ pindy_dotfiles.username }}
{% endfor %}

{# vim:set et sw=2 ts=4 ft=sls: #}
