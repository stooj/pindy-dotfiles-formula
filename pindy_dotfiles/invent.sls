{% from "pindy_dotfiles/map.jinja" import pindy_dotfiles with context %}

{% set username = salt['pillar.get']('pindy_dotfiles:username', 'pindy') %}
{% set home = "/home/%s" % username %}

{{ username }}_dotfiles_invent_directory_managed:
  file.directory:
    - name: {{ home }}/programming/invent
    - user: {{ username }}
    - group: {{ username }}
    - dir_mode: 755

{% set invent_repos = {
  'calculate-pay-withholding': 'git@odo.stooj.org:invent-the-world/calculate-pay-withholding.git'
  }
%}

  
{% for name, address in invent_repos.items() %}
{{ username }}_dotfiles_{{ name|replace('-', '_')|replace('.', '_') }}_cloned:
  git.latest:
    - name: {{ address }}
    - target: {{ home }}/programming/invent/{{ name }}
    - user: {{ username }}
    - branch: master
    - identity:
      - salt://ssh/files/deploy_key
    - require:
      - user: {{ username }}
      - file: {{ home }}/programming
      - {{ username }}_dotfiles_invent_directory_managed
{% endfor %}
