{% from "pindy_dotfiles/map.jinja" import pindy_dotfiles with context %}

{% set username = salt['pillar.get']('pindy_dotfiles:username', 'pindy') %}
{% set home = "/home/%s" % username %}

pindy_dotfiles_nextcloud_config_managed:
  file.managed:
    - name: {{ home }}/.config/Nextcloud/nextcloud.cfg
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0644
    - makedirs: True
    - replace: False
    - require:
      - user: {{ username }}

pindy_dotfiles_nextcloud_config_settings_managed:
  ini.options_present:
    - name: {{ home }}/.config/Nextcloud/nextcloud.cfg
    - sections:
        General:
          confirmExternalStorage: true
          newBigFolderSizeLimit: 1000
          optionalDesktopNotifications: true
          optionalServerNotifications: true
          useNewBigFolderSizeLimit: true
        Accounts:
          "0\\Folders\\1\\ignoreHiddenFiles": "false"
          "0\\Folders\\1\\localPath": "{{ home }}/nextcloud/itw/"
          "0\\url": "{{ pindy_dotfiles.nextcloudconfig.zero.serverurl }}"
          "1\\Folders\\2\\ignoreHiddenFiles": "false"
          "1\\Folders\\2\\localPath": "{{ home }}/nextcloud/goodteith/"
          "1\\url": "{{ pindy_dotfiles.nextcloudconfig.one.serverurl }}"
        BWLimit:
          useDownloadLimit: -1
          useUploadLimit: -1
    - require:
      - user: {{ username }}

pindy_dotfiles_nextcloud_directory_exists:
  file.directory:
    - name: {{ home }}/nextcloud
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0755
    - require:
      - user: {{ username }}

pindy_dotfiles_nextcloud_itw_directory_exists:
  file.directory:
    - name: {{ home }}/nextcloud/itw
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0744
    - require:
      - pindy_dotfiles_nextcloud_directory_exists

pindy_dotfiles_nextcloud_goodteith_directory_exists:
  file.directory:
    - name: {{ home }}/nextcloud/goodteith
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0744
    - require:
      - pindy_dotfiles_nextcloud_directory_exists

{{ username }}_dotfiles_nextcloud_autostart_file_exists:
  file.managed:
    - name: {{ home }}/.config/autostart/nextcloud.desktop
    - source: salt://pindy_dotfiles/files/nextcloud/nextcloud.desktop
    - makedirs: True
    - user: {{ username }}
    - group: {{ username }}
