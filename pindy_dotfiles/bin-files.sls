{% from "pindy_dotfiles/map.jinja" import pindy_dotfiles with context %}

{% set username = salt['pillar.get']('pindy_dotfiles:username', 'pindy') %}
{% set home = "/home/%s" % username %}

{{ username }}_dotfiles_bin_files:
  file.recurse:
    - name: {{ home }}/bin
    - source: salt://{{ username }}_dotfiles/files/bin-files
    - require:
      - user: {{ username }}
    - user: {{ username }}
    - group: {{ username }}
    - dir_mode: 755
    - file_mode: 0744

{#
{% if salt['grains.get']('role', '') == 'personal-computer' %}
{{ username }}_dotfiles_pc_bin_files:
  file.recurse:
    - name: {{ home }}/bin
    - source: salt://{{ username }}_dotfiles/files/pc-bin-files
    - require:
      - user: {{ username }}
    - user: {{ username }}
    - group: {{ username }}
    - dir_mode: 755
    - file_mode: 0744
{% endif %}
#}
