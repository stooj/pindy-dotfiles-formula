{% from "pindy_dotfiles/map.jinja" import pindy_dotfiles with context %}

{% set username = salt['pillar.get']('pindy_dotfiles:username', 'pindy') %}
{% set home = "/home/%s" % username %}

{{ username }}_dotfiles_pass_installed:
  pkg.installed:
    - pkgs: {{ pindy_dotfiles.pkgs.pass|yaml }}

{{ username }}_dotfiles_qtpass_installed:
  pkg.installed:
    - pkgs: {{ pindy_dotfiles.pkgs.qtpass|yaml }}

{{ username }}_dotfiles_pass_repo_cloned:
  git.latest:
    - name: {{ pindy_dotfiles.passconfig.git_repo }}
    - target: {{ home }}/.password-store
    - user: pindy
    - identity:
      - salt://ssh/files/deploy_key
    - require:
      - user: {{ username }}

{{ username }}_dotfiles_qtpass_config_exists:
  file.managed:
    - name: {{ home }}/.config/IJHack/QtPass.conf
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0600
    - makedirs: True
    - replace: False
    - require:
      - user: {{ username }}

{{ username }}_dotfiles_qtpass_config_managed:
  ini.options_present:
    - name: {{ home }}/.config/IJHack/QtPass.conf
    - sections:
        General:
          addGPGId: true
          autoPull: true
          autoPush: true
          passStore: {{ home }}/.password-store/
          useGit: true
          usePass: true
          usePwgen: true
    - require:
      - {{ username }}_dotfiles_qtpass_config_exists


