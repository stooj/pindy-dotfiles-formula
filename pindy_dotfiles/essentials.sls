{% from "pindy_dotfiles/map.jinja" import pindy_dotfiles with context %}

{% set username = pindy_dotfiles.username %}
pindy_dotfiles_ensure_user_exists:
  user.present:
    - name: {{ username }}
