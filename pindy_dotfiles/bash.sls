{% from "pindy_dotfiles/map.jinja" import pindy_dotfiles with context %}

{% set username = salt['pillar.get']('pindy_dotfiles:username', 'pindy') %}
{% set home = "/home/%s" % username %}

pindy_dotfiles_manage_bashrc:
  file.managed:
    - name: {{ home }}/.bashrc
    - source: salt://pindy_dotfiles/templates/bash/bashrc.jinja
    - template: jinja
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0644
    - require:
      - user: {{ username }}
