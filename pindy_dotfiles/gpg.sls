{% from "pindy_dotfiles/map.jinja" import pindy_dotfiles with context %}

{% set username = salt['pillar.get']('pindy_dotfiles:username', 'pindy') %}
{% set home = "/home/%s" % username %}

{{ username }}_dotfiles_gpg_installed:
  pkg.installed:
    - pkgs: {{ pindy_dotfiles.pkgs.gpg|yaml }}

{{ username }}_dotfiles_gpg_directory_managed:
  file.directory:
    - name: {{ home }}/.gnupg
    - user: {{ username }}
    - group: {{ username }}
    - dir_mode: 700
    - recurse:
      - user
      - group
    - require:
      - user: {{ username }}

{% for key in pindy_dotfiles.gpgkeys %}
{{ username }}_dotfiles_{{ key }}_key_managed:
  gpg.present:
    - name: {{ key }}
    - keys: {{ key }}
    - keyserver: hkps://hkps.pool.sks-keyservers.net
    - user: {{ username }}
    - trust: ultimately
    - require:
      - user: {{ username }}
{% endfor %}

{% for keyid, key in pindy_dotfiles.gpgsecretkeys.items() %}
{{ username }}_dotfiles_{{ keyid }}_secret_key_saved:
  file.managed:
    - name: {{ home }}/private-key
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0600
    - contents_pillar: {{ 'pindy_dotfiles:gpgsecretkeys:' ~ keyid }}
    - unless: 'gpg --homedir {{ home }}/.gnupg --list-secret-keys {{ keyid }}'
    - require:
      - user: {{ username }}

{{ username }}_dotfiles_{{ keyid }}_secret_key_added:
  cmd.run:
    - name: gpg --batch --import < {{ home }}/private-key
    - cwd: {{ home }}
    - runas: {{ username }}
    - onlyif: 'ls {{ home }}/private-key'
    - require:
      - {{ username }}_dotfiles_{{ keyid }}_secret_key_saved

{{ username }}_dotfiles_{{ keyid }}_secret_key_removed:
  file.absent:
    - name: {{ home }}/private-key
    - onlyif: 'ls {{ home }}/private-key'
    - require:
      - {{ username }}_dotfiles_{{ keyid }}_secret_key_added
{% endfor %}
