==============
pindy_dotfiles
==============

Install dotfiles for pindy

.. note::

    See the full `Salt Formulas installation and usage instructions
    <http://docs.saltstack.com/en/latest/topics/development/conventions/formulas.html>`_.


Available states
================

.. contents::
    :local:

``pindy_dotfiles``
------------------

TODO - add description of this state

``pindy_dotfiles.install``
-----------------------

Handles installation of pindy_dotfiles

``pindy_dotfiles.config``
-----------------------

Handles configuration of pindy_dotfiles


Configured applications
=======================

Template
========

This formula was created from a cookiecutter template.

See https://github.com/mitodl/saltstack-formula-cookiecutter.
