stooj_dotfiles formula
======================

201807 (2018-07-30)
-------------------

- First release
- Added essential user checking
- Added git configuration
- Added plasma configuration
